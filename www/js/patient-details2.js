$(document).ready(function() {
                  fillPatDetail2();
//                  fillHMO();
                  });


function fillPatDetail2()
{
     patId = localStorage.getItem('id');
//    alert("patientId:" + patId);
    
    $.ajax({
           url: "http://vaihademo.com/delonapps/patient_datas.php",
           type: "POST",
           headers: {
           "code": "ESnPmSWqWK43aFcYnYKXAGf43N42uqbe"
           },
           data: "id=" + patId,
           
           success: function(result)
           {
//                alert("yes: "+JSON.stringify(result));
           
                $('#allrgy').val(result[0].allergies);
                $('#bgp').val(result[0].blood_group);
                $('#brh').val(result[0].blood_rhesus);
                $('#genotyp').val(result[0].genotype);
                $('#pec').val(result[0].pre_existing_conditions);
                $('#tellnum').val(result[0].teller_number);
                $('#bank').val(result[0].bank);
                $('#bankbr').val(result[0].bank_branch);
                $('#dop').val(result[0].date_of_payment);
                $('#psd').val(result[0].proposed_startdate);
                $('#pcp').val(result[0].primary_care_provider);

                localStorage.setItem('allr_gy', result[0].allergies);
                localStorage.setItem('b_gp', result[0].blood_group);
                localStorage.setItem('b_rh', result[0].blood_rhesus);
                localStorage.setItem('geno_type', result[0].genotype);
                localStorage.setItem('pe_c', result[0].pre_existing_conditions);
                localStorage.setItem('tell_num', result[0].teller_number);
                localStorage.setItem('ban_k', result[0].bank);
                localStorage.setItem('b_br', result[0].bank_branch);
                localStorage.setItem('do_p', result[0].date_of_payment);
                localStorage.setItem('ps_d', result[0].proposed_startdate);
                localStorage.setItem('pc_p', result[0].primary_care_provider);

//                pd_subId = localStorage.getItem('sub_id');
//                pd_fname = localStorage.getItem('f_name');
//                pd_lname = localStorage.getItem('l_name');
//                pd_mname = localStorage.getItem('m_name');
//                pd_gender = localStorage.getItem('gen_r');
//                pd_mstatus = localStorage.getItem('stat');
//                pd_dob = localStorage.getItem('dob');
//                pd_numSt = localStorage.getItem('num_st');
//                pd_area = localStorage.getItem('are_a');
//                pd_ward = localStorage.getItem('war_d');
//                pd_lga = localStorage.getItem('lg_a');
//                pd_state = localStorage.getItem('stat_e');
//                pd_phone = localStorage.getItem('ph_one');
//                pd_email = localStorage.getItem('e_mail');
//                pd_allergy = localStorage.getItem('allr_gy');
//                pd_bloodgp = localStorage.getItem('b_gp');
//                pd_bloodrh = localStorage.getItem('b_rh');
//                pd_genotype = localStorage.getItem('geno_type');
//                pd_pec = localStorage.getItem('pe_c');
//                pd_tellnum = localStorage.getItem('tell_num');
//                pd_bank = localStorage.getItem('ban_k');
//                pd_bankbr = localStorage.getItem('b_br');
//                pd_dop = localStorage.getItem('do_p');
//                pd_psd = localStorage.getItem('ps_d');
//                pd_pcp = localStorage.getItem('pc_p');
           },
           error: function(result)
           {
               navigator.notification.alert("error","","Delon LLC");
           }
           });
}


//function fillHMO()
//{
//    clientId = localStorage.getItem('client-id');
//    alert("cli_id:" + clientId);
//    $.ajax({
//           url: "http://vaihademo.com/delonapps/get_hmo_users.php?client_id=1",
//           type: 'GET',
//           data: "client_id="+clientId,
//           beforeSend: function(xhr) {
//           xhr.setRequestHeader('CODE', 'ESnPmSWqWK43aFcYnYKXAGf43N42uqbe');
//           },
//           contentType: 'application/json',
//           success: function(res) {
//           alert("yeppp:"+JSON.stringify(res));
//           
//           var str = '';
//           var typeOne = "",
//           typeTwo = "";
//           for (i = 0; i < res.length; i++) {
//           typeOne = res[i].id;
//           typeTwo = res[i].username;
//           str += '<option value="' + typeOne + '">' + typeTwo + '</option>';
//           }
//           $('#hmo').html(str);
//           },
//           error: function(err) {
//           alert("Error: " + err);
//           },
//           });
//}


function saveDetails2()
{
    //    var lastId = localStorage.getItem('last_id');
    var allergy_s2 = $('#allrgy').val();
    var bloodgp_s2 = $('#bgp').val();
    var bloodrh_s2 = $('#brh').val();
    var genotype_s2 = $('#genotyp').val();
    var exCond_s2 = $('#pec').val();

    var tellnum_s2 = $('#tellnum').val();
    var bank_s2 = $('#bank').val();
    var bankbr_s2 = $('#bankbr').val();
    var dop_s2 = $('#dop').val();
    var psd_s2 = $('#psd').val();
    var pcp_s2 = $('#pcp').val();
    
    // var f = $.dob.parseDate("Y-m-d", dobirth);
    var today = new Date();
    var b1 = new Date(dop_s2);
    var b2 = new Date(psd_s2);
    
    // alert("Date:"+f);
    //    var msDateA = Date.parse(today.getFullYear(),today.getMonth()+1,today.getDate());
    //    var msDateB = Date.parse(b1.getFullYear(),b1.getMonth()+1,b1.getDate());
    // alert(today.getTime() +":"+b.getTime());
    
    
    if(today.getTime() > b1.getTime())
    {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter valid payment date", "", "Delon LLC");
        return;
    }
    
    if(today.getTime() > b2.getTime())
    {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter valid start date", "", "Delon LLC");
        return;
    }
    

    localStorage.setItem('allr_gy_s2', allergy_s2);
    localStorage.setItem('b_gp_s2', bloodgp_s2);
    localStorage.setItem('b_rh_s2', bloodrh_s2);
    localStorage.setItem('geno_type_s2', genotype_s2);
    localStorage.setItem('pe_c_s2', exCond_s2);
    localStorage.setItem('tell_num_s2', tellnum_s2);
    localStorage.setItem('ban_k_s2', bank_s2);
    localStorage.setItem('b_br_s2', bankbr_s2);
    localStorage.setItem('do_p_s2', dop_s2);
    localStorage.setItem('ps_d_s2', psd_s2);
    localStorage.setItem('pc_p_s2', pcp_s2);

    pds12_subId = localStorage.getItem('sub_id_s1');
    pds12_hmo = localStorage.getItem('h_mo_s1');
    pds12_fname = localStorage.getItem('f_name_s1');
    pds12_lname = localStorage.getItem('l_name_s1');
    pds12_mname = localStorage.getItem('m_name_s1');
    pds12_gender = localStorage.getItem('gen_r_s1');
    pds12_mstatus = localStorage.getItem('stat_s1');
    pds12_dob = localStorage.getItem('dob_s1');
    pds12_numSt = localStorage.getItem('num_st_s1');
    pds12_area = localStorage.getItem('are_a_s1');
    pds12_ward = localStorage.getItem('war_d_s1');
    pds12_lga = localStorage.getItem('lg_a_s1');
    pds12_state = localStorage.getItem('stat_e_s1');
    pds12_phone = localStorage.getItem('ph_one_s1');
    pds12_email = localStorage.getItem('e_mail_s1');

    pds2_allergy = localStorage.getItem('allr_gy_s2');
    pds2_bloodgp = localStorage.getItem('b_gp_s2');
    pds2_bloodrh = localStorage.getItem('b_rh_s2');
    pds2_genotype = localStorage.getItem('geno_type_s2');
    pds2_pec = localStorage.getItem('pe_c_s2');
    pds2_tellnum = localStorage.getItem('tell_num_s2');
    pds2_bank = localStorage.getItem('ban_k_s2');
    pds2_bankbr = localStorage.getItem('b_br_s2');
    pds2_dop = localStorage.getItem('do_p_s2');
    pds2_psd = localStorage.getItem('ps_d_s2');
    pds2_pcp = localStorage.getItem('pc_p_s2');

    $.ajax({
           url: "http://vaihademo.com/delonapps/edit_patient_details.php",
           type: "POST",
           headers: {
           "code": "ESnPmSWqWK43aFcYnYKXAGf43N42uqbe"
           },
           data: "last_id="+patId+"&first_name="+pds12_fname+"&last_name="+pds12_lname+"&middle_name="+pds12_mname+"&gender="+pds12_gender+"&marital_status="+pds12_mstatus+"&dob="+pds12_dob+"&street1="+pds12_numSt+"&area="+pds12_area+"&ward="+pds12_ward+"&LGA="+pds12_lga+"&state="+pds12_state+"&email="+pds12_email+"&phone="+pds12_phone+"&allergies="+pds2_allergy+"&blood_group="+pds2_bloodgp+"&blood_rhesus="+pds2_bloodrh+"&genotype="+pds2_genotype+"&pre_existing_conditions="+pds2_pec+"&teller_number="+pds2_tellnum+"&bank="+pds2_bank+"&date_of_payment="+pds2_dop+"&proposed_startdate="+pds2_psd+"&primary_care_provider="+pds2_pcp+"&bank_branch="+pds2_bankbr+"&hmo="+pds12_hmo,
           
           success: function(result)
           {
//           alert("yes: "+JSON.stringify(result));
               navigator.notification.alert("Details updated successfully","","Delon LLC");
           },
           error: function(result)
           {
               navigator.notification.alert("error","","Delon LLC");
           }
           });
}

function back()
{
    location.href="patient-details1.html";
}

function logout()
{
    location.href="index.html";
}