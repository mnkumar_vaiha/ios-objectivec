var charactersAllowed = 250;
var charactersAllowed_cmct = 1500;


$(document).ready(function ()
                  {
                    //   id_pat_pcp = localStorage.getItem('id_pcp');
                    subid_pat_pcp = localStorage.getItem('sub_id_pcp');
                    fname_pat_pcp = localStorage.getItem('f_name');
                    lname_pat_pcp = localStorage.getItem('l_name');
                    dob_pat_pcp = localStorage.getItem('dob');
                    patimg_pat_pcp = localStorage.getItem('pat_img');
                    allergy_pat_pcp = localStorage.getItem('allr_gy_pcp');
                    bgp_pat_pcp = localStorage.getItem('b_gp_pcp');
                    brh_pat_pcp = localStorage.getItem('b_rh_pcp');
                    geno_pat_pcp = localStorage.getItem('geno_type_pcp');
                    pec_pat_pcp = localStorage.getItem('pe_c_pcp');
                    cmct_pat_pcp = localStorage.getItem('cm_ct_pcp');
                    cm_pat_pcp = localStorage.getItem('c_m_pcp');

                    $('#subsId_pat').val(subid_pat_pcp);
                    $('#name_ip').val(fname_pat_pcp);
                    // $('#pcp_allergies').val(lname_pat_pcp);
                    $('#dob_ip').val(dob_pat_pcp);

                    var str = '';
                    str += '<div class="profImg">';
//                    str +='<img src="'+ patimg_pat_pcp +'"width="70" class="round" id="img" style="height:55px;width:55px;">';
                  
                  
                  str += '<img src="'+patimg_pat_pcp+'" class="round" style="height:55px;width:55px;" data-toggle="modal" data-target="#myModal">';
                  
                  str += '<div class="container">';
                  //           <!-- Modal -->
                  str += ' <div class="modal fade" id="myModal" role="dialog">';
                  str += '<div class="modal-dialog">';
                  //           <!-- Modal content-->
                  str += ' <div class="modal-content">';
                  str += ' <div class="modal-header">';
                  str += '<button type="button" class="close" data-dismiss="modal">&times;</button>';
                  str += '<h4 class="modal-title" style = "color:#333">'+fname_pat_pcp + " " + lname_pat_pcp+'</h4>';
                  str += '</div>';
                  str += '<div class="modal-body" style="text-align:center">';
                  //           str += '<p>Some text in the modal.</p>';
                  str += '<img src="'+patimg_pat_pcp+'" style="width:80%;" >';
                  str += ' </div>';
                  str += '</div>';
                  str += '</div>';
                  str += '</div>';
                  str += '</div>';

                  
                  
                    str += '</div>';
                    $('.profImg').html(str);

                    $('#pcp_allergies').val(allergy_pat_pcp);
                    $('#pcp_bloodgp').val(bgp_pat_pcp);
                    $('#pcp_bloodrh').val(brh_pat_pcp);
                    $('#pcp_geno').val(geno_pat_pcp);
                    $('#pcp_pec').val(pec_pat_pcp);
                    $('#pcp_cmct').val(cmct_pat_pcp);
                    $('#pcp_cm').val(cm_pat_pcp);


                    var left = charactersAllowed - (allergy_pat_pcp.length);
                    $('#counter_allergy').text(left);

                    var left = charactersAllowed - (geno_pat_pcp.length);
                    $('#counter_geno').text(left);

                    var left = charactersAllowed - (pec_pat_pcp.length);
                    $('#counter_pec').text(left);

                      var left = charactersAllowed_cmct - (cmct_pat_pcp.length);
                      $('#counter_cmct').text(left);

                      var left = charactersAllowed - (cm_pat_pcp.length);
                      $('#counter_cm').text(left);

                    key();
                   });


function key()
{
    $('#pcp_allergies').keyup(function ()
                              {
                              var left = charactersAllowed - $(this).val().length;
                              if (left < 0)
                              {
                              alert("Maximum charecter level reached");
                              left = 0;
                              }
                              $('#counter_allergy').text(left);
                              });
    
    $('#pcp_geno').keyup(function ()
                         {
                         var left = charactersAllowed - $(this).val().length;
                         if (left < 0)
                         {
                         alert("Maximum charecter level reached");
                         left = 0;
                         }
                         $('#counter_geno').text(left);
                         });
    
    $('#pcp_pec').keyup(function ()
                        {
                        var left = charactersAllowed - $(this).val().length;
                        if (left < 0)
                        {
                        alert("Maximum charecter level reached");
                        left = 0;
                        }
                        $('#counter_pec').text(left);
                        });
    
    $('#pcp_cmct').keyup(function ()
                         {
                         var left = charactersAllowed_cmct - $(this).val().length;
                         if (left < 0)
                         {
                         alert("Maximum charecter level reached");
                         left = 0;
                         }
                         $('#counter_cmct').text(left);
                         });
    
    $('#pcp_cm').keyup(function ()
                       {
                       var left = charactersAllowed - $(this).val().length;
                       if (left < 0)
                       {
                       alert("Maximum charecter level reached");
                       left = 0;
                       }
                       $('#counter_cm').text(left);
                       });
}

function savemeddetails()
{

    id_pat_pcp = localStorage.getItem('id_pcp');
    

    var allergy_med_pcp = $('#pcp_allergies').val();
    var bgp_med_pcp = $('#pcp_bloodgp').val();
    var brh_med_pcp = $('#pcp_bloodrh').val();
    var geno_med_pcp = $('#pcp_geno').val();
    var pec_med_pcp = $('#pcp_pec').val();
    var cmct_med_pcp = $('#pcp_cmct').val();
    var cm_med_pcp = $('#pcp_cm').val();
    
    if (allergy_med_pcp == '') {

        navigator.notification.alert("Fill in the allergies","","Delon LLC");
        return;
    } else if (bgp_med_pcp == '') {
        navigator.notification.alert("Fill in the blood group","","Delon LLC");
        return;
    } else if (brh_med_pcp == '') {
        navigator.notification.alert("Fill in the blood rhesus","","Delon LLC");
        return;
    } else if (geno_med_pcp == '') {
        navigator.notification.alert("Fill in the genotype","","Delon LLC");
        return;
    } else if (pec_med_pcp == '') {
        navigator.notification.alert("Fill in the pre-existing conditions","","Delon LLC");
        return;
    } else if (cmct_med_pcp == '') {
        navigator.notification.alert("Fill in the current medication & treatment","","Delon LLC");
        return;
    } else if (cm_med_pcp == '') {
        navigator.notification.alert("Fill in the current medication","","Delon LLC");
        return;
    }
    document.getElementById("imgProgress").style.display = "block";

    $.ajax({
           url: "http://vaihademo.com/delonapps/pcp_update_patient.php",
           type: "POST",
           headers: {
           "code": "ESnPmSWqWK43aFcYnYKXAGf43N42uqbe"
           },
           data: "allergies="+allergy_med_pcp+"&blood_group="+bgp_med_pcp+"&blood_rhesus="+brh_med_pcp+"&genotype="+geno_med_pcp+"&pre_existing_conditions="+pec_med_pcp+"&cur_treatments="+cmct_med_pcp+"&cur_medication="+cm_med_pcp+"&id="+id_pat_pcp,
           success: function(result)
           {
//               alert("yes" + JSON.stringify(result));
               document.getElementById("imgProgress").style.display = "none";
               if(result.status == 'success'){
                   navigator.notification.alert("Saved successfully","","Delon LLC");
               }
                else{
                   navigator.notification.alert("Invalid inputs","","Delon LLC");
           }

           },
           error: function(result)
           {
               document.getElementById("imgProgress").style.display = "none";

               navigator.notification.alert("Invalid inputs","","Delon LLC");
           }
           });
}

function back()
{
    location.href = "pcp-loginPatDetail.html";
}

function logout()
{
    location.href = "index.html";
}
