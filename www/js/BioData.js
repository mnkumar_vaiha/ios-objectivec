$(document).ready(function() {
                  fillDetailsreg();
                  });

function fillDetailsreg()
{
    fn_reg = localStorage.getItem('f_n');
    mn_reg = localStorage.getItem('m_n');
    ln_reg = localStorage.getItem('l_n');
    gd_reg = localStorage.getItem('g_d');
    mt_reg = localStorage.getItem('m_t');
    dob_reg = localStorage.getItem('do_b');


    $('#firstName').val(fn_reg);
     $('#middleName').val(mn_reg);
     $('#lastName').val(ln_reg);
     $('#gender').val(gd_reg);
     $('#maritalStatus').val(mt_reg);
     $('#dob').val(dob_reg);
}


function bioDataNextBtn() {
    document.getElementById("imgProgress").style.display = "block";

    
    var lastId = localStorage.getItem('last_id');
    if('last_id' == "")
    {
        location.href = "upload-photo.html";
    }
    var frstname = $('#firstName').val();
    var midname = $('#middleName').val();
    var lstname = $('#lastName').val();
    var gendr = $('#gender').val();
    var marital = $('#maritalStatus').val();
    var dobirth = $('#dob').val();
    
    // var f = $.dob.parseDate("Y-m-d", dobirth);
    var today = new Date();
    var b = new Date(dobirth);
    
    // alert("Date:"+f);
    var msDateA = Date.parse(today.getFullYear(),today.getMonth()+1,today.getDate());
    var msDateB = Date.parse(b.getFullYear(),b.getMonth()+1,b.getDate());
    // alert(today.getTime() +":"+b.getTime());


    //alert("m:"+marital+" g:"+gendr);
    if ($('#firstName').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your First name","","Delon LLC");
        return;
    } else if ($('#middleName').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your middle name","","Delon LLC");
        return;
    } else if ($('#lastName').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your last name","","Delon LLC");
        return;
    } else if ($('#gender').val() == null) {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please select your gender","","Delon LLC");
        return;
    } else if ($('#maritalStatus').val() == null) {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please select your marital status","","Delon LLC");
        return;
    } else if ($('#dob').val() == 0) {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your Date of Birth","","Delon LLC");
        return;
    }
    else if(today.getTime() < b.getTime())
    {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter valid date of birth", "", "Delon LLC");
        return;
    }
    
    localStorage.setItem('f_n', frstname);
    localStorage.setItem('m_n', midname);
    localStorage.setItem('l_n', lstname);
    localStorage.setItem('g_d', gendr);
    localStorage.setItem('m_t', marital);
    localStorage.setItem('do_b', dobirth);


    
    $.ajax({
           url: "http://vaihademo.com/delonapps/bio_data.php",
           type: "POST",
           headers: {
           "code": "ESnPmSWqWK43aFcYnYKXAGf43N42uqbe"
           },
           data: "last_id=" + lastId + "&first_name=" + frstname + "&last_name=" + lstname + "&middle_name=" + midname + "&gender=" + gendr + "&marital_status=" + marital + "&dob=" + dobirth,
           success: function(result) {
//           alert("yes" + JSON.stringify(result));
           if (result.status == "success") {
           document.getElementById("imgProgress").style.display = "none";
           location.href = "address.html";
           }
           },
           error: function(result) {
               document.getElementById("imgProgress").style.display = "none";
               navigator.notification.alert("Invalid inputs","","Delon LLC");
           }
           });
}