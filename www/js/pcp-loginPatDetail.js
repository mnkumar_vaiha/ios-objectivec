
var x = document.getElementById("subid_ip");
var y = document.getElementById("name_ip");
var z = document.getElementById("dob_ip");

x.addEventListener("focus", myFocusFunction, true);
y.addEventListener("focus", myFocusFunction1, true);
z.addEventListener("focus", myFocusFunction1, true);

function myFocusFunction()
{
    document.getElementById("subid_ip").style.backgroundColor = "";
    document.getElementById("name_ip").style.backgroundColor = "lightgray";
    document.getElementById("dob_ip").style.backgroundColor = "lightgray";
}

function myFocusFunction1() {
    document.getElementById("name_ip").style.backgroundColor = "";
    document.getElementById("dob_ip").style.backgroundColor = "";
    document.getElementById("subid_ip").style.backgroundColor = "lightgray";
}


// ========================================================


function getDetailsPat()
{
    
    var subid_ofPat = $("#subid_ip").val();
    var name_ofPat = $("#name_ip").val();
    var dob_ofPat = $("#dob_ip").val();

    if (!(subid_ofPat == 0 && (name_ofPat == '' && dob_ofPat == 0))) {

        if(subid_ofPat != 0)
        {
//        alert("sub");
            document.getElementById("imgProgress").style.display = "block";

            $.ajax({
                url: "http://vaihademo.com/delonapps/get_patient_details.php",
                type: "POST",
                headers: {
                    "code": "ESnPmSWqWK43aFcYnYKXAGf43N42uqbe"
                },
                data: "subscriber_id="+subid_ofPat,
                success: function(result)
                {
//                   alert("yes_pcp1" + JSON.stringify(result));
                    if (result.status == "fail")
                    {
                        document.getElementById("imgProgress").style.display = "none";

                        navigator.notification.alert("Invalid inputs","","Delon LLC");
                    }
                    else {
                        localStorage.setItem('id_pcp', result[0].id);
                        localStorage.setItem('sub_id_pcp', result[0].subscriber_id);
                        localStorage.setItem('f_name', result[0].first_name);
                        localStorage.setItem('l_name', result[0].last_name);
                        localStorage.setItem('dob', result[0].dob);
                        localStorage.setItem('pat_img', result[0].patient_image);
                        localStorage.setItem('allr_gy_pcp', result[0].allergies);
                        localStorage.setItem('b_gp_pcp', result[0].blood_group);
                        localStorage.setItem('b_rh_pcp', result[0].blood_rhesus);
                        localStorage.setItem('geno_type_pcp', result[0].genotype);
                        localStorage.setItem('pe_c_pcp', result[0].pre_existing_conditions);
                        localStorage.setItem('cm_ct_pcp', result[0].cur_medi_treatments);
                        localStorage.setItem('c_m_pcp', result[0].cur_medication);

                        document.getElementById("imgProgress").style.display = "none";

                        location.href = "pcp-patdetails.html";
                    }

                },
                error: function(result)
                {
                    document.getElementById("imgProgress").style.display = "none";

                    navigator.notification.alert("Invalid inputs","","Delon LLC");
                }
            });
        }
        else if(name_ofPat != '' && dob_ofPat != 0)
        {
//        alert("2nd");
            document.getElementById("imgProgress").style.display = "block";

            // alert("first_name="+name_ofPat+"dob="+dob_ofPat);
            $.ajax({
                url: "http://vaihademo.com/delonapps/get_patient_details.php",
                type: "POST",
                headers: {
                    "code": "ESnPmSWqWK43aFcYnYKXAGf43N42uqbe"
                },
                data: "first_name=" + name_ofPat + "&dob=" + dob_ofPat,
                success: function(result)
                {
//                   alert("yesss_pcp2:" + JSON.stringify(result));
                    if (result.status == "fail")
                    {
                        document.getElementById("imgProgress").style.display = "none";
                        navigator.notification.alert("Invalid inputs","","Delon LLC");
                    }
                    else {
                        localStorage.setItem('id_pcp', result[0].id);
                        localStorage.setItem('sub_id_pcp', result[0].subscriber_id);
                        localStorage.setItem('f_name', result[0].first_name);
                        localStorage.setItem('l_name', result[0].last_name);
                        localStorage.setItem('dob', result[0].dob);
                        localStorage.setItem('pat_img', result[0].patient_image);
                        localStorage.setItem('allr_gy_pcp', result[0].allergies);
                        localStorage.setItem('b_gp_pcp', result[0].blood_group);
                        localStorage.setItem('b_rh_pcp', result[0].blood_rhesus);
                        localStorage.setItem('geno_type_pcp', result[0].genotype);
                        localStorage.setItem('pe_c_pcp', result[0].pre_existing_conditions);
                        localStorage.setItem('cm_ct_pcp', result[0].cur_medi_treatments);
                        localStorage.setItem('c_m_pcp', result[0].cur_medication);
                        // navigator.notification.alert("Invalid inputs","","Delon LLC");
                        document.getElementById("imgProgress").style.display = "none";
                        location.href = "pcp-patdetails.html";

                    }
                },
                error: function(result)
                {
                    document.getElementById("imgProgress").style.display = "none";
                    navigator.notification.alert("Invalid inputs","","Delon LLC");
                }
            });
        }
    } else {
        navigator.notification.alert("Enter either Subscriber id or first name with DoB","","Delon LLC");
        // alert("Enter either Subscriber id or first name with DoB");
        return;
    }
    

}
