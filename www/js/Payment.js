$(document).ready(function() {
                  fillDetailspay();
                  });

function fillDetailspay()
{
    tn_pay = localStorage.getItem('t_n');
    bk_pay = localStorage.getItem('b_k');
    bb_pay = localStorage.getItem('b_b');
    pd_pay = localStorage.getItem('p_d');
    sd_pay = localStorage.getItem('s_d');
    pcp_pay = localStorage.getItem('pc_p');

     $('#tellerNum').val(tn_pay);
     $('#bank').val(bk_pay);
     $('#bankBranch').val(bb_pay);
     $('#paymentDate').val(pd_pay);
     $('#startDate').val(sd_pay);
     $('#pcp').val(pcp_pay);
}



function paymentNextBtn() {
    document.getElementById("imgProgress").style.display = "block";
    
    var lastId = localStorage.getItem('last_id');
    var tellerNum_pay = $('#tellerNum').val();
    var bank_pay = $('#bank').val();
    var bankbranch_pay = $('#bankBranch').val();
    var paymentDate_pay = $('#paymentDate').val();
    var startDate_pay = $('#startDate').val();
    var pcp_pay = $('#pcp').val();
    
    if ($('#tellerNum').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your Teller number","","Delon LLC");
        return;
    } else if ($('#bank').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your bank name","","Delon LLC");
        return;
    } else if ($('#bankBranch').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your bank's branch","","Delon LLC");
        return;
    } else if ($('#paymentDate').val() == 0) {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please specify the payment date","","Delon LLC");
        return;
    } else if ($('#startDate').val() == 0) {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please specify your start date","","Delon LLC");
        return;
    } else if ($('#pcp').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your PCP details","","Delon LLC");
        return;
    }
    
    // var f = $.dob.parseDate("Y-m-d", dobirth);
    var today = new Date();
    var b1 = new Date(paymentDate_pay);
    var b2 = new Date(startDate_pay);
    
    // alert("Date:"+f);
    //    var msDateA = Date.parse(today.getFullYear(),today.getMonth()+1,today.getDate());
    //    var msDateB = Date.parse(b1.getFullYear(),b1.getMonth()+1,b1.getDate());
    // alert(today.getTime() +":"+b.getTime());
    
    
    if(today.getTime() > b1.getTime())
    {
        
        document.getElementById("imgProgress").style.display = "none";
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter valid payment date","","Delon LLC");
        return;
    }
    
    if(today.getTime() > b2.getTime())
    {
        document.getElementById("imgProgress").style.display = "none";
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter valid start date","","Delon LLC");
        return;
    }
    
    
        localStorage.setItem('t_n', tellerNum_pay);
        localStorage.setItem('b_k', bank_pay);
        localStorage.setItem('b_b', bankbranch_pay);
        localStorage.setItem('p_d', paymentDate_pay);
        localStorage.setItem('s_d', startDate_pay);
        localStorage.setItem('pc_p', pcp_pay);
    
    
    $.ajax({
           url: "http://vaihademo.com/delonapps/payment_register.php",
           type: "POST",
           headers: {
           "code": "ESnPmSWqWK43aFcYnYKXAGf43N42uqbe"
           },
           data: "teller_number=" + tellerNum_pay + "&bank=" + bank_pay + "&date_of_payment=" + paymentDate_pay + "&proposed_startdate=" + startDate_pay + "&primary_care_provider=" + pcp_pay + "&last_id=" + lastId + "&bank_branch=" + bankbranch_pay,
           success: function(result) {
//           alert("yes" + JSON.stringify(result));
           if (result.status == "success") {
           localStorage.setItem('subscriberId', result.subscriber_id);
           localStorage.setItem('reg_status', result.patient_status);
           document.getElementById("imgProgress").style.display = "none";
           location.href = "idAndStatus.html";
           }
           },
           error: function(result) {
               navigator.notification.alert("Invalid inputs","","Delon LLC");
           }
           });
}

function back()
{
    location.href = "history.html";
}