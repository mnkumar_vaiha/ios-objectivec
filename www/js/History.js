$(document).ready(function() {
                  fillDetailshis();
                  });

function fillDetailshis()
{
    al_his = localStorage.getItem('a_l');
    bg_his = localStorage.getItem('b_g');
    br_his = localStorage.getItem('b_r');
    gt_his = localStorage.getItem('g_t');
    ec_his = localStorage.getItem('e_c');

     $('#allergy').html(al_his);
     $('#bloodgp').val(bg_his);
     $('#bloodrh').val(br_his);
     $('#genotyp').val(gt_his);
     $('#existingCondi').html(ec_his);
}


function historyNextBtn() {
    document.getElementById("imgProgress").style.display = "block";
    
    var lastId = localStorage.getItem('last_id');
    var allergy_hist = $('#allergy').val();
    var bloodgp_hist = $('#bloodgp').val();
    var bloodrh_hist = $('#bloodrh').val();
    var genotype_hist = $('#genotyp').val();
    var existingConditions_hist = $('#existingCondi').val();
    
    if ($('#allergy').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your allergies","","Delon LLC");
        return;
    } else if ($('#bloodgp').val() == null) {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please select your blood group","","Delon LLC");
        return;
    } else if ($('#bloodrh').val() == null) {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please select your blood's rh factor","","Delon LLC");
        return;
    } else if ($('#genotyp').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your genotype","","Delon LLC");
        return;
    } else if ($('#existingCondi').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your pre-existing conditions","","Delon LLC");
        return;
    }
    
        localStorage.setItem('a_l', allergy_hist);
        localStorage.setItem('b_g', bloodgp_hist);
        localStorage.setItem('b_r', bloodrh_hist);
        localStorage.setItem('g_t', genotype_hist);
        localStorage.setItem('e_c', existingConditions_hist);
    
    $.ajax({
           url: "http://vaihademo.com/delonapps/medical_history.php",
           
           type: "POST",
           headers: {
           "code": "ESnPmSWqWK43aFcYnYKXAGf43N42uqbe"
           },
           
           data: "last_id=" + lastId + "&allergies=" + allergy_hist + "&blood_group=" + bloodgp_hist + "&blood_rhesus=" + bloodrh_hist + "&genotype=" + genotype_hist + "&pre_existing_conditions=" + existingConditions_hist,
           
           success: function(result) {
//           alert("yes" + JSON.stringify(result));
           if (result.status == "success") {
           document.getElementById("imgProgress").style.display = "none";
           location.href = "Payment.html";
           }
           },
           error: function(result) {
               document.getElementById("imgProgress").style.display = "none";
               navigator.notification.alert("Invalid inputs","","Delon LLC");
           }
           });
}

function back()
{
    location.href = "address.html";
}