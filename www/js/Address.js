$(document).ready(function() {
                  fillDetailsaddr();
                  });

function fillDetailsaddr()
{
    ns_addr = localStorage.getItem('n_s');
    ar_addr = localStorage.getItem('a_e');
    wa_addr = localStorage.getItem('w_a');
    lga_addr = localStorage.getItem('lg_a');
    st_addr = localStorage.getItem('s_t');
    ph_addr = localStorage.getItem('ph_o');
    ml_addr = localStorage.getItem('m_l');

    
    $('#numAndStreet').val(ns_addr);
    $('#area').val(ar_addr);
    $('#ward').val(wa_addr);
    $('#lga').val(lga_addr);
    $('#state').val(st_addr);
    $('#phone').val(ph_addr);
    $('#mailid').val(ml_addr);

}


function addrNextBtn() {
    document.getElementById("imgProgress").style.display = "block";

    
    var lastId = localStorage.getItem('last_id');
    var numAndSt_addr = $('#numAndStreet').val();
    var area_addr = $('#area').val();
    var ward_addr = $('#ward').val();
    var lga_addr = $('#lga').val();
    var state_addr = $('#state').val();
    var phone_addr = $('#phone').val();
    var mail_addr = $('#mailid').val();
    
    if ($('#numAndStreet').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter Number & Street name","","Delon LLC");
        return;
    } else if ($('#area').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your Area","","Delon LLC");
        return;
    } else if ($('#ward').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter the ward number","","Delon LLC");
        return;
    } else if ($('#lga').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your Local Government","","Delon LLC");
        return;
    } else if ($('#state').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter the State","","Delon LLC");
        return;
    } else if ($('#phone').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your Number","","Delon LLC");
        return;
    } else if ($('#mailid').val() == '') {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter your Mail address","","Delon LLC");
        return;
    }
    
    var pattern = /^\(\d{3}\)\s*\d{3}(?:-|\s*)\d{4}$/;
    if (pattern.test(phone_addr))
    {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter vaild phone number","","Delon LLC");
        return;
    }
    
    if (phone_addr.length>13)
    {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Enter only 13 digits","","Delon LLC");
        return;
    }
    
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (!filter.test(mail_addr))
    {
        document.getElementById("imgProgress").style.display = "none";
        navigator.notification.alert("Please enter vaild email address","","Delon LLC");
        return;

    }
    
    localStorage.setItem('n_s', numAndSt_addr);
    localStorage.setItem('a_e', area_addr);
    localStorage.setItem('w_a', ward_addr);
    localStorage.setItem('lg_a', lga_addr);
    localStorage.setItem('s_t', state_addr);
    localStorage.setItem('ph_o', phone_addr);
    localStorage.setItem('m_l', mail_addr);

 
    $.ajax({
           url: "http://vaihademo.com/delonapps/contact_address.php",
           type: "POST",
           headers: {
           "code": "ESnPmSWqWK43aFcYnYKXAGf43N42uqbe"
           },
           data: "last_id="+ lastId +"&street1=" + numAndSt_addr + "&area=" + area_addr + "&ward=" + ward_addr + "&LGA=" + lga_addr + "&state=" + state_addr + "&email=" + mail_addr + "&phone=" + phone_addr,
           success: function(result) {
//           alert("yes" + JSON.stringify(result));
               if (result.status == "success") {
           document.getElementById("imgProgress").style.display = "none";
               location.href = "history.html";
               }
           },
           error: function(result) {
               navigator.notification.alert("Invalid inputs","","Delon LLC");
           }
           });
}

function back()
{
    location.href = "bio-data.html";
   
}